def is_triangle(a, b, c):
    # Check if all sides are greater than zero
    if a <= 0 or b <= 0 or c <= 0:
        return False
    
    # Check the triangle inequality theorem
    return (a + b > c) and (a + c > b) and (b + c > a)
    
